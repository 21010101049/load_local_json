import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomePage(),
    );
  }
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder<String>(
        builder: (context, snapshot) {
          if(snapshot.hasData){
            return Text(jsonDecode(snapshot.data.toString())["name"]);
          }else{
            return CircularProgressIndicator();
          }
        },
        future: rootBundle.loadString("/loadJson/loadjson.json.dart"),
      ),
    );
  }
}
